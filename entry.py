#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
from os import path as ospath

from aiohttp import web

from demo import create_app
from demo.settings import load_config


__all__ = ('app',)


_DEFAULT_CONF_FILE = ospath.join(
    ospath.abspath(ospath.dirname(__file__)),
    '.conf.json')

arg_parser = argparse.ArgumentParser(description='File storage server')
arg_parser.add_argument('-c', '--config', type=str, default=_DEFAULT_CONF_FILE)

args = arg_parser.parse_args()

config = load_config(args.config)
app = create_app(config=config)


if __name__ == '__main__':
    web.run_app(
        app, host=config.get('host', 'localhost'),
        port=config.get('port', 5000))
