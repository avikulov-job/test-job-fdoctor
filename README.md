# Task information

1)  Написать сервис, с функциональностью:
    a.  upload -> загрузить  файл, сохранить файл в файловой системе,
        сформировать уникальную ссылку для скачивания файла
    b.  check -> проверить наличие ранее загруженного файла по ссылке
    c.  download -> скачать ранее загруженный файл используя ссылку
2)  реализовать ограничение скорости upload и download
3)  реализовать этот сервис в формате multithreading или async
4)  написать тесты
5)  завернуть в docker (ubuntu | alpine)

Плюсом будет:
    - соответствие pep8
    - документирование кода
    - аннотация типов
    - наличие логов
    - нагрузочный тест
    - обработка возможных ошибок

Ограничение:
В реализации основного кода сервиса
использовать только стандартные библиотеки Python 3.6 (3.7)

# Run
```bash
python entry.py
```

# Usage example
## File upload
### Request
```bash
curl -i -X POST -H "Content-Type: multipart/form-data" -F "file=path/to/file.ext;filename=test.ext" [host]:[port]/upload
```

### Response
```json
{
  "file-check-url": "/check/fb8b99ffff5",
  "file-download-url": "/download/fb8b99ffff5",
  "status": 200,
  "reason": "Ok",
  "errors": ""
}
```

## File check
### Request
```bash
curl -X GET [host]:[port]/check/fb8b99ffff5
```

### Response
```json
{
  "exists": true,
  "filename": "file.ext",
  "file-download-url": "/download/fb8b99ffff5"
}
```

## File download
### Request
```bash
curl -o "file.ext" -L --max-redirs 1 -X GET [host]:[port]/download/fb8b99ffff5
```

### Response
#### File or 404 error
