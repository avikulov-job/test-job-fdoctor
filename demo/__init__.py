#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from demo.app import create_app


__all__ = ('create_app',)
