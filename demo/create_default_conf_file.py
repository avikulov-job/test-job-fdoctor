#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

from os import path as ospath


__all__ = ('main',)


def main() -> None:
    default_conf_file = ospath.join(
        ospath.abspath(ospath.dirname(__file__)),
        'default_conf.json')

    default_configuration = {
        'Host': 'localhost',
        'Port': 5000,
    }

    with open(default_conf_file, 'wt', encoding='utf-8') as f:
        json.dump(default_configuration, f)

    print(f'Default configuration file is created: {default_conf_file}')
    return None


if __name__ == '__main__':
    main()
