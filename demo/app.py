#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from aiohttp import web

from demo.routes import get_urls


__all__ = ('create_app',)


async def create_app(config: dict) -> web.Application:
    app = web.Application()

    # app['config'] = config
    app.add_routes(get_urls())

    return app
