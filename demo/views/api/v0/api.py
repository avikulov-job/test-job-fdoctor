#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import hashlib
import json
import mimetypes
import os
import shutil
import tempfile
from os import path as ospath

import yarl
from aiohttp import web
from typing import Tuple

from demo.settings import (
    CHUNK_SIZE,
    STORAGE_DIR, TMP_FILES_DIR,
    HASHES_FILENAME)


__all__ = (
    'FileUpload', 'FileCheck',
    'FileDownloadByHash', 'FileDownloadByName')


# {filehash: filename}
if ospath.exists(HASHES_FILENAME) and ospath.isfile(HASHES_FILENAME):
    with open(HASHES_FILENAME, mode='rt', encoding='utf-8') as f:
        _FILES_NAMES = json.load(f)
else:
    _FILES_NAMES = dict()
    for idx, cur_filename in enumerate(os.listdir(STORAGE_DIR), start=1):
        if ospath.isfile(ospath.join(STORAGE_DIR, cur_filename)):
            _FILES_NAMES[cur_filename] = f'file-{idx}'

# initializing mime types
mimetypes.init()


# extra functions
def _is_file_exists_by_hash(filehash: str) -> Tuple[bool, str, str]:
    """
    Check, file exists or not

    :param filehash: file hash
    :return: tuple(file exists, filename, full file path)
    """
    file_exists = False
    filename = _FILES_NAMES.get(filehash, '')
    full_path = ospath.join(STORAGE_DIR, filehash)

    if filename and ospath.exists(full_path) and ospath.isfile(full_path):
        file_exists = True

    return (file_exists, filename, full_path)


def _get_filehash_from_request(request: web.Request) -> str:
    """
    Get file hash from request object

    :param request: request object
    :return: file hash or empty string
    """
    result = request.match_info.get('filehash', '')
    return result


def _get_url_file_check_by_hash(
        request: web.Request, filehash: str) -> yarl.URL:
    """
    Generate URL for file checking

    :param request: request object
    :param filehash: file hash, for generating check link
    :return: yarl.URL instance
    """
    result = request.app.router['file-check'].url_for(filehash=filehash)
    return result


def _get_url_file_download_by_hash(
        request: web.Request, filehash: str) -> yarl.URL:
    """
    Generate URL for file downloading

    :param request: request object
    :param filehash: file hash, for generating check link
    :return: yarl.URL instance
    """
    result = request.app.router['file-download-by-hash'].url_for(
        filehash=filehash)

    return result


def _get_url_file_download_by_name(
        request: web.Request, dirname: str, filename: str) -> yarl.URL:
    """
    Generate URL for file downloading

    :param request: request object
    :param dirname: name of directory where file storing
        (just name, not absolute path!)
    :param filename: file name in directory
    :return: yarl.URL instance
    """
    result = request.app.router['file-download-by-name'].url_for(
        dirname=dirname, filename=filename)

    return result


# VIEWS
class FileUpload(web.View):
    async def post(self) -> web.Response:
        reader = await self.request.multipart()

        field = True
        while field:
            field = await reader.next()
            if field and field.name == 'file':
                break

        if not field:
            data = {
                'errors': 'Cannot find "file" field!',
                'status': 400,
            }
            return web.json_response(data=data)

        filename = field.filename
        # We cannot rely on Content-Length if transfer is chunked.
        size = 0
        filehash = hashlib.sha3_224()

        (fd, tmp_filename) = tempfile.mkstemp(dir=TMP_FILES_DIR, text=False)
        with open(tmp_filename, mode='wb') as f:
            while True:
                # 8192 bytes by default.
                chunk = await field.read_chunk(CHUNK_SIZE)

                if not chunk:
                    break

                size += len(chunk)
                f.write(chunk)
                filehash.update(chunk)
                continue

        # close the file descriptor
        os.close(fd=fd)
        filehash = filehash.hexdigest()

        fname_in_storage = ospath.join(STORAGE_DIR, filehash)
        os.replace(tmp_filename, fname_in_storage)

        _FILES_NAMES[filehash] = filename
        with open(HASHES_FILENAME, mode='wt', encoding='utf-8') as f:
            json.dump(_FILES_NAMES, f)

        # url for file checking/downloading
        file_chk_url = _get_url_file_check_by_hash(self.request, filehash)
        file_dwd_url = _get_url_file_download_by_hash(self.request, filehash)

        data = {
            'file-check-url': str(file_chk_url),
            'file-download-url': str(file_dwd_url),
            'status': 200,
            'reason': 'Ok',
            'errors': '',
        }
        return web.json_response(data=data)


class FileCheck(web.View):
    async def get(self) -> web.Response:
        filehash = _get_filehash_from_request(self.request)
        (file_exists, filename, full_path) = _is_file_exists_by_hash(filehash)
        dwd_url = _get_url_file_download_by_hash(self.request, filehash)

        data = {
            'exists': file_exists,
            'filename': filename,
            'file-download-url': str(dwd_url) if file_exists else '',
        }
        return web.json_response(data=data)


class FileDownloadByHash(web.View):
    async def get(self) -> None:
        filehash = _get_filehash_from_request(self.request)
        (file_exists, filename, full_path) = _is_file_exists_by_hash(filehash)

        # file not found
        if not file_exists:
            raise web.HTTPNotFound

        # copy file with normal name to temp directory
        # because may be way when upload 2 files with
        # the same names but different hashes.
        tmp_dir = tempfile.mkdtemp(dir=TMP_FILES_DIR)
        new_full_path = ospath.join(tmp_dir, filename)
        shutil.copy2(full_path, new_full_path)

        tmp_dir = ospath.split(tmp_dir)[-1]
        # redirect to downloading file by name
        new_url = self.request.app.router['file-download-by-name'].url_for(
            dirname=tmp_dir, filename=filename)
        raise web.HTTPFound(new_url)


class FileDownloadByName(web.View):
    async def get(self) -> web.StreamResponse:
        filename = self.request.match_info.get('filename', '')

        dirname = self.request.match_info.get('dirname', '')
        dirname = ospath.join(TMP_FILES_DIR, dirname)

        full_path = ospath.join(dirname, filename)
        # if file not exists
        if not (ospath.exists(full_path) and ospath.isfile(full_path)):
            raise web.HTTPNotFound

        # mime type, and content encoding
        (mtype, cencoding) = mimetypes.guess_type(full_path, strict=True)
        if mtype is None:
            mtype = 'application/octet-stream'
            cencoding = None

        headers = {'Content-Type': mtype}
        if cencoding:
            headers['Content-Encoding'] = cencoding

        response = web.StreamResponse(headers=headers)
        await response.prepare(self.request)

        with open(full_path, mode='rb') as f:
            while True:
                chunk = f.read(CHUNK_SIZE)

                if not chunk:
                    break

                await response.write(chunk)
                continue
            # end of file
            await response.write_eof()

        # remove temp directory
        shutil.rmtree(dirname)

        return response
