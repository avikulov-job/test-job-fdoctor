#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import logging

import os
from os import path as ospath


__all__ = (
    'load_config', 'CHUNK_SIZE',
    'STORAGE_DIR', 'TMP_FILES_DIR',
    'HASHES_FILENAME')


_LOGGER = logging.getLogger(__name__)

# 64 Kb
CHUNK_SIZE = 65536
CONF_FILE_EXTENSION = 'json'

# directories
_PROJECT_DIR = ospath.abspath(ospath.dirname(__file__))
_BASE_DIR = ospath.dirname(_PROJECT_DIR)

STORAGE_DIR = ospath.join(_PROJECT_DIR, 'storage')
TMP_FILES_DIR = ospath.join(_PROJECT_DIR, 'tmp')

HASHES_FILENAME = ospath.join(TMP_FILES_DIR, '.hashes.json')

# creating dirs if its does not exists
for cur_dir in (STORAGE_DIR, TMP_FILES_DIR):
    os.makedirs(cur_dir, exist_ok=True)


def is_conf_file_exists(filename: str) -> bool:
    result = bool(
        ospath.exists(filename) and
        ospath.isfile(filename) and
        filename.split('.')[-1] == CONF_FILE_EXTENSION)

    _LOGGER.debug(f'filename "{filename}" is configuration: {result}')
    return result


def load_config(config_file: str) -> dict:
    default_file = ospath.join(ospath.dirname(__file__), 'default_conf.json')

    if is_conf_file_exists(default_file):
        with open(default_file, 'rt') as f:
            config = json.load(f)

        # dict.keys.lower: Host, HOst, etc. => host
        config = {k.lower(): v for k, v in config.items()}

    else:
        config = dict()

    if is_conf_file_exists(config_file):
        with open(config_file, 'rt') as f:
            cur_conf = json.load(f)

        # dict.keys.lower: Host, HOst, etc. => host
        cur_conf = {k.lower(): v for k, v in cur_conf.items()}
        config.update(cur_conf)

    return config
