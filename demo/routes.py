#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from aiohttp import web

from demo.views.api import v0 as views


__all__ = ('get_urls',)


_API_VERSIONS_PREFIXES = {
    0: '',
    1: '/api/v1',
}


def get_api_url_by_version(version: int, url: str) -> str:
    """

    :param version: API version
    :param url: URL string which concatenates with API prefix.
    :return: String represented as "[api prefix]/[url]"
    """

    # preprocessing url
    while url.startswith('/') and len(url) > 1:
        url = url[1:]

    result = f'{_API_VERSIONS_PREFIXES[version]}/{url}'
    return result


def get_urls() -> tuple:
    result = (
        # file upload
        web.view(
            get_api_url_by_version(0, 'upload'),
            views.FileUpload, name='file-upload'),
        # file check
        web.view(
            get_api_url_by_version(0, 'check/{filehash}'),
            views.FileCheck, name='file-check'),
        # file download by hash
        web.view(
            get_api_url_by_version(0, 'download/{filehash}'),
            views.FileDownloadByHash, name='file-download-by-hash'),
        # file download by name
        web.view(
            get_api_url_by_version(0, 'files/{dirname}/{filename}'),
            views.FileDownloadByName, name='file-download-by-name'),
    )

    return result
